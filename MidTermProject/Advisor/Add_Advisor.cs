﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidTermProject.Advisor
{
    public partial class Add_Advisor : Form
    {
        public Add_Advisor()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e) // add an advisor
        {
            bool check = true;
            int gender = -1;

            if (check == true)
            {
                if (string.IsNullOrEmpty(salary.Text) || string.IsNullOrEmpty(firstName.Text) || string.IsNullOrEmpty(lastName.Text) || string.IsNullOrEmpty(contact.Text) || string.IsNullOrEmpty(email.Text))
                {
                    check = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            try
            {
                if (check == true)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO Person (FirstName, LastName, Contact, Email, DateOfBirth, Gender) VALUES (@FirstName,@LastName, @Contact,@Email,@DOB, @Gender)", con);
                    SqlCommand cmd1 = new SqlCommand("INSERT INTO Advisor (Id, Designation, Salary) VALUES ((SELECT Id FROM Person WHERE FirstName = @FirstName AND LastName=@LastName AND Contact=@Contact AND Email=@Email AND DateOfBirth=@DOB AND Gender=@Gender), @Designation, @Salary)", con);
                    cmd.Parameters.AddWithValue("@FirstName", firstName.Text);
                    cmd.Parameters.AddWithValue("@LastName", lastName.Text);
                    cmd.Parameters.AddWithValue("@Contact", contact.Text);
                    cmd.Parameters.AddWithValue("@Email", email.Text);
                    cmd1.Parameters.AddWithValue("@FirstName", firstName.Text);
                    cmd1.Parameters.AddWithValue("@LastName", lastName.Text);
                    cmd1.Parameters.AddWithValue("@Contact", contact.Text);
                    cmd1.Parameters.AddWithValue("@Email", email.Text);
                    cmd1.Parameters.AddWithValue("@Salary", salary.Text);
                    cmd1.Parameters.AddWithValue("@Designation", designationText.SelectedIndex + 6);

                    if (genderText.SelectedItem == "Male")
                    {
                        gender = 1;
                    }
                    if (genderText.SelectedItem == "Female")
                    {
                        gender = 2;
                    }

                    dateTimePicker.Format = DateTimePickerFormat.Custom;
                    dateTimePicker.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@Gender", gender);
                    cmd.Parameters.AddWithValue("@DOB", dateTimePicker.Text);
                    cmd1.Parameters.AddWithValue("@Gender", gender);
                    cmd1.Parameters.AddWithValue("@DOB", dateTimePicker.Text);

                    cmd.ExecuteNonQuery();
                    cmd1.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");
                    this.Hide();
                    Form f = new Form1();
                    f.Show();
                }
            }

            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            this.Hide();
            f.Show();
        }
    }
}
