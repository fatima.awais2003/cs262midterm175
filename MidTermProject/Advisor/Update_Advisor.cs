﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MidTermProject.Advisor
{
    public partial class Update_Advisor : Form
    {
        public int ID_text;
        public Update_Advisor(int ID, int Salary, int Designation, int Gender, string FN, string LN, string Email, string Contact, string DOB)
        {
            InitializeComponent();
            ID_text = ID;
            genderText.SelectedIndex = Gender - 1;
            firstName.Text = FN;
            lastName.Text = LN;
            email.Text = Email;
            contact.Text = Contact;
            salary.Text = Salary.ToString();   
            designation.SelectedIndex = Designation - 6;
            dateTimePicker.Value = DateTime.Parse(DOB);

        }

        private void Update_Advisor_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            this.Hide();
            f.Show();
        }

        private void button3_Click(object sender, EventArgs e) // update
        {
            if (ID_text != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update Person " +
                        " set FirstName = @FirstName, LastName = @LastName, Contact = @Contact, Email = @Email, DateOfBirth = @DOB, Gender = @Gender where Id = @ID " +
                        " update Advisor set Salary = @Salary, Designation = @Designation where Id = @ID ", con);
                    cmd.Parameters.AddWithValue("@FirstName", firstName.Text);
                    cmd.Parameters.AddWithValue("@LastName", lastName.Text);
                    cmd.Parameters.AddWithValue("@Contact", contact.Text);
                    cmd.Parameters.AddWithValue("@Email", email.Text);
                    cmd.Parameters.AddWithValue("@ID", ID_text);
                    cmd.Parameters.AddWithValue("@Gender", genderText.SelectedIndex + 1);
                    cmd.Parameters.AddWithValue("@Salary", int.Parse(salary.Text));
                    cmd.Parameters.AddWithValue("@Designation", designation.SelectedIndex + 6);
                    dateTimePicker.Format = DateTimePickerFormat.Custom;
                    dateTimePicker.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@DOB", dateTimePicker.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully update");

                    Form f = new View_Advisor();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }
    }
}
