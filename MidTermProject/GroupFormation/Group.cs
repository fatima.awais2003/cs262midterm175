﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MidTermProject.GroupFormation
{
    public partial class Group : Form
    {
        public Group()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Group_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool flag = true;

            if(flag == true)
            {
                if(dateTimePicker1 == null)
                {
                    flag = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            if(flag == true)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO [Group](Created_On) VALUES (@date)", con);

                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@date", dateTimePicker1.Value);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully Saved");

                    Form f = new Form1();
                    this.Hide();
                    f.Show();
                }

                catch(Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            this.Hide();
            f.Show();
        }
    }
}
