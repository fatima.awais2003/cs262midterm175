﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidTermProject.GroupFormation
{
    public partial class Update_Group : Form
    {
        public int ID_text;
        
        public Update_Group(int ID, string Date)
        {
            InitializeComponent();
            ID_text = ID;
            dateTimePicker1.Value = DateTime.Parse(Date);
        }

        private void Update_Group_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form f = new View_Group();
            this.Hide();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e) // updating
        {
            if (ID_text != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update [Group] " +
                        " set Created_On = @Created_On where Id = @ID " , con);
       
                    cmd.Parameters.AddWithValue("@ID", ID_text);
                  
                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@Created_On", dateTimePicker1.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully update");

                    Form f = new View_Group();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }
    }
}
