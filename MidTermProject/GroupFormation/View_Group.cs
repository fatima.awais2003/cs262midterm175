﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidTermProject.GroupFormation
{
    public partial class View_Group : Form
    {
        public int ID_text;
        public string Date;
        public View_Group()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e) // back
        {
            Form f = new Form1();
            this.Hide();
            f.Show();
        }

        private void View_Group_Load(object sender, EventArgs e) // view
        {
            this.WindowState = FormWindowState.Maximized;
            dataGridView1.ReadOnly = true;
            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id , Created_On \r\nfrom [Group]");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void button2_Click(object sender, EventArgs e) // reload
        {
            dataGridView1.ReadOnly = true;
            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id , Created_On \r\nfrom [Group]");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID_text = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            Date = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
        }

        private void button4_Click(object sender, EventArgs e) // delete
        {
            if (ID_text != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Delete From [Group] Where Id = @Id");
                    cmd.Parameters.AddWithValue("@Id", ID_text);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");

                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }

            else
            {
                MessageBox.Show("Select a record to delete");
            }

        }

        private void button3_Click(object sender, EventArgs e) // update
        {
            if (Date == null || ID_text == 0)
            {
                MessageBox.Show("Select a record");
            }
            if (Date != null || ID_text != 0)
            {
                try
                {
                    Form f = new Update_Group(ID_text, Date);
                    this.Hide();
                    f.Show();
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }
    }
    
}
