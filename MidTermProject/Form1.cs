﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidTermProject.Students_CRUD;
using MidTermProject.Advisor;
using MidTermProject.Project;
using MidTermProject.Evaluation;
using MidTermProject.GroupFormation;
using MidTermProject.GroupStudent;
using MidTermProject.Group_Evaluation;
using MidTermProject.GroupProject;
using MidTermProject.ProjectAdvisor;

namespace MidTermProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click_1(object sender, EventArgs e) // add a student
        {
            Form f = new Student();
            f.Show();
            this.Hide();
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void button2_Click(object sender, EventArgs e) // opens add advisor form
        {
          
            Form f = new View_Students();
            this.Hide();
            f.Show();
        }

        private void button3_Click(object sender, EventArgs e) // view and edit students
        {
            Form f = new Add_Advisor();
            this.Hide();
            f.Show();
        }

        private void button4_Click(object sender, EventArgs e) // view and edit advisor
        {
            Form f = new View_Advisor();
            this.Hide();
            f.Show();
        }

        private void button5_Click(object sender, EventArgs e) // adds a new project
        {
            Form f = new Add_Project();
            this.Hide();
            f.Show();
        }

        private void button6_Click(object sender, EventArgs e) // view a project
        {
            Form f = new View_Projects();
            this.Hide();
            f.Show();
        }

        private void button8_Click(object sender, EventArgs e) // add evaluation
        {
            Form f = new Add_Evaluation();
            this.Hide();
            f.Show();
        }

        private void button9_Click(object sender, EventArgs e) // view and edit evaluations
        {
            Form f = new View_Evaluation();
            this.Hide();
            f.Show();
        }

        private void button10_Click(object sender, EventArgs e) // create group
        {
            Form f = new Group();
            this.Hide();
            f.Show();
        }

        private void button11_Click(object sender, EventArgs e) // view group
        {
            Form f = new View_Group();
            this.Hide();
            f.Show();
        }

        private void button12_Click(object sender, EventArgs e) // add studnets in groups
        {
            Form f = new GroupStudent.GroupStudent();
            this.Hide();
            f.Show();

        }

        private void button13_Click(object sender, EventArgs e) // view students in groups
        {
            Form f = new View_GroupStudent();
            this.Hide();
            f.Show();
        }

        private void button14_Click(object sender, EventArgs e) // group evaluation
        {
            Form f = new Add_GroupEvaluation();
            this.Hide();
            f.Show();

        }

        private void button15_Click(object sender, EventArgs e) // view group evaluations
        {
            Form f = new View_GroupEvaluations();
            this.Hide();
            f.Show();
        }

        private void button16_Click(object sender, EventArgs e) // assign projects to groups
        {
            Form f = new AssignProject();
            this.Hide();
            f.Show();
        }

        private void button17_Click(object sender, EventArgs e) // view group projects
        {
            Form f = new View_GroupProject();
            this.Hide();
            f.Show();
        }

        private void button18_Click(object sender, EventArgs e) // add project advisor
        {
            Form f = new Assign_ProjectAdvisor();
            this.Hide();
            f.Show();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            Form f = new View_ProjectAdvisor();
            this.Hide();
            f.Show();
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Form f = new Generate_Reports();
            this.Hide();
            f.Show();
        }
    }
}
