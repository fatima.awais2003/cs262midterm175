﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MidTermProject.ProjectAdvisor
{
    public partial class Assign_ProjectAdvisor : Form
    {
        public Assign_ProjectAdvisor()
        {
            InitializeComponent();
        }

        private void Assign_ProjectAdvisor_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void advisor_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from Advisor";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Advisor");
                    advisor.DisplayMember = "Id";
                    advisor.ValueMember = "Id";
                    advisor.DataSource = ds.Tables["Advisor"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void project_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from Project";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Project");
                    project.DisplayMember = "Id";
                    project.ValueMember = "Id";
                    project.DataSource = ds.Tables["Project"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            this.Hide();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e) // add
        {
            bool check = true;
            int role_advisor = 0;

            if (check == true)
            {
                if (string.IsNullOrEmpty(dateTimePicker1.Text) || string.IsNullOrEmpty(project.Text) || string.IsNullOrEmpty(advisor.Text) || string.IsNullOrEmpty(role.Text))
                {
                    check = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            try
            {
                if (check == true)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO ProjectAdvisor(AdvisorId,ProjectId,AdvisorRole,AssignmentDate) VALUES (@AdvisorId,@ProjectId, @AdvisorRole,@AssignmentDate)", con);
                    cmd.Parameters.AddWithValue("@AdvisorId", advisor.SelectedValue);
                    cmd.Parameters.AddWithValue("@ProjectId", project.SelectedValue);

                    if (role.Text == "Industry Advisor")
                    {
                        role_advisor = 14;
                        cmd.Parameters.AddWithValue("@AdvisorRole", role_advisor);

                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@AdvisorRole", role.SelectedIndex + 11);
                    }

                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePicker1.Text);

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");

                    Form f = new Form1();
                    this.Hide();
                    f.Show();
                }
            }

            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }

        }

        private void role_Click(object sender, EventArgs e) // roles
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                
                var id_advisor = advisor.SelectedValue;
                var id_project = project.SelectedValue;

                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand query = new SqlCommand("select Lookup.Value from Lookup where Id > 10 except(select Lookup.Value from ProjectAdvisor join Lookup on Lookup.Id = ProjectAdvisor.AdvisorRole where ProjectId = @ProjectId) ", con);
                    query.Parameters.AddWithValue("@ProjectId", project.SelectedValue);

                    SqlDataAdapter da = new SqlDataAdapter(query);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Lookup");
                    role.DisplayMember = "Value";
                    role.ValueMember = "Value";
                    role.DataSource = ds.Tables["Lookup"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
