﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidTermProject.ProjectAdvisor
{
    public partial class Update_ProjectAdvisor : Form
    {
        public int AID, PID, ad_role;
        public string date;

        private void Update_ProjectAdvisor_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void button2_Click(object sender, EventArgs e) // update
        {
            if (AID != 0)
            {
                int role_advisor = 0;
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update ProjectAdvisor set AdvisorRole = @AdvisorRole, AssignmentDate = @AssignmentDate where AdvisorId = @AdvisorId AND ProjectId = @ProjectId", con);

                    cmd.Parameters.AddWithValue("@AdvisorId", AID);
                    cmd.Parameters.AddWithValue("@ProjectId", PID);

                    if (role.Text == "Industry Advisor")
                    {
                        role_advisor = 14;
                        cmd.Parameters.AddWithValue("@AdvisorRole", role_advisor);

                    }
                    if (role.Text == "Main Advisor")
                    {
                        role_advisor = 11;
                        cmd.Parameters.AddWithValue("@AdvisorRole", role_advisor);

                    }
                    if (role.Text == "Co-Advisror")
                    {
                        role_advisor = 12;
                        cmd.Parameters.AddWithValue("@AdvisorRole", role_advisor);

                    }
                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePicker1.Text);

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully update");

                    Form f = new View_ProjectAdvisor();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }

        private void role_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand query = new SqlCommand("select Lookup.Value from Lookup where Id > 10 except(select Lookup.Value from ProjectAdvisor join Lookup on Lookup.Id = ProjectAdvisor.AdvisorRole where ProjectId = @ProjectId) ", con);
                    query.Parameters.AddWithValue("@ProjectId", int.Parse(project.Text));

                    SqlDataAdapter da = new SqlDataAdapter(query);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Lookup");
                    role.DisplayMember = "Value";
                    role.ValueMember = "Value";
                    role.DataSource = ds.Tables["Lookup"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public Update_ProjectAdvisor(int advisorID, int projectID, int advisorROLE, string assignmentDate)
        {
            InitializeComponent();
            dateTimePicker1.Value = DateTime.Parse(assignmentDate);
            AID = advisorID;
            PID = projectID;
            ad_role = advisorROLE;

            advisor.Items.Add(AID);
            project.Items.Add(PID);
            role.Items.Add(ad_role);
        }

        private void button1_Click(object sender, EventArgs e) // back
        {
            Form f = new View_ProjectAdvisor();
            this.Hide();
            f.Show();
        }
    }
}
