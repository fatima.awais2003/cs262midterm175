﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidTermProject.ProjectAdvisor
{
   
    public partial class View_ProjectAdvisor : Form
    {
        public int AID = 0, PID = 0, ad_role = 0;
        public string date;
        public View_ProjectAdvisor()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) // back
        {
            Form f = new Form1();
            this.Hide();
            f.Show();
        }

        private void View_ProjectAdvisor_Load(object sender, EventArgs e) // view
        {
            this.WindowState = FormWindowState.Maximized;
            dataGridView1.ReadOnly = true;

            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select AdvisorId, ProjectId, L.Value, AssignmentDate from ProjectAdvisor as PA join Lookup as L on PA.AdvisorRole = L.Id");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void button2_Click(object sender, EventArgs e) // reload
        {
            dataGridView1.ReadOnly = true;

            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select AdvisorId, ProjectId, L.Value, AssignmentDate from ProjectAdvisor as PA join Lookup as L on PA.AdvisorRole = L.Id");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void button3_Click(object sender, EventArgs e) // update
        {
            if (AID == 0 || PID == 0 || date == null)
            {
                MessageBox.Show("Select a record");
            }

            if (AID != 0 || PID != 0 || ad_role != 0 || date != null)
            {
                try
                {
                    Form f = new Update_ProjectAdvisor(AID, PID, ad_role, date);
                    this.Hide();
                    f.Show();
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (AID != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("delete from ProjectAdvisor where ProjectId = @ProjectId AND AdvisorId = @AdvisorId");
                    cmd.Parameters.AddWithValue("@AdvisorId", AID);
                    cmd.Parameters.AddWithValue("@ProjectId", PID);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");

                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
            else
            {
                MessageBox.Show("Select a record");
            }
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            AID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            PID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
            
            if (dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString() == "Main Advisor")
            {
                ad_role = 11;
            }
            else if (dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString() == "Co-Advisror")
            {
                ad_role = 12;
            }
            else if(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString() == "Industry Advisor")
            {
                ad_role = 14; 
            }

            date = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
        }
    }
}
