﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MidTermProject.GroupStudent
{
    public partial class GroupStudent : Form
    {
        public int GId, StdId;
        public GroupStudent()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
        }

        private void button1_Click(object sender, EventArgs e) // back button
        {
            Form f = new Form1();
            this.Hide();
            f.Show();
        }

        private void student_Click(object sender, EventArgs e) // clicks to check student ids 
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "SELECT s.Id FROM Student s LEFT JOIN (SELECT * FROM GroupStudent GS1 WHERE GS1.AssignmentDate = ( SELECT MAX(GS2.AssignmentDate) FROM GroupStudent GS2 WHERE GS2.StudentId = GS1.StudentId)) recent ON s.Id = recent.StudentID WHERE recent.Status = 4 OR recent.GroupID IS NULL";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Student");
                    student.DisplayMember = "Id";
                    student.ValueMember = "Id";
                    student.DataSource = ds.Tables["Student"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void group_Click(object sender, EventArgs e) // show groups
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from [Group]";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "[Group]");
                    group.DisplayMember = "Id";
                    group.ValueMember = "Id";
                    group.DataSource = ds.Tables["[Group]"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e) // add student in group
        {
            bool check = true;
            int status_option = -1;

            if (check == true)
            {
                if (string.IsNullOrEmpty(dateTimePicker1.Text) || string.IsNullOrEmpty(status.Text) || string.IsNullOrEmpty(student.Text) || string.IsNullOrEmpty(group.Text))
                {
                    check = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            try
            {
                if (check == true)
                {
                    //SqlCommand cmd = new SqlCommand("INSERT INTO GroupStudent(GroupId,StudentId,Status,AssignmentDate) VALUES (@GroupId,@StudentId, @Status,@AssignmentDate)", con);

                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO GroupStudent(GroupId,StudentId,Status,AssignmentDate) VALUES (@GroupId,@StudentId, @Status,@AssignmentDate)", con);
                    cmd.Parameters.AddWithValue("@GroupId", group.SelectedValue);
                    cmd.Parameters.AddWithValue("@StudentId", student.SelectedValue);
                    
                    if (status.SelectedItem == "Active")
                    {
                        status_option = 3;
                    }
                    if (status.SelectedItem == "InActive")
                    {
                        status_option = 4;
                    }

                    cmd.Parameters.AddWithValue("@Status", status_option);

                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePicker1.Text);
                    
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");

                    Form f = new Form1();
                    this.Hide();
                    f.Show();
                }
            }

            catch (Exception error)
            {
                MessageBox.Show("The Student ID has already been added");
            }
        }

    }
}
