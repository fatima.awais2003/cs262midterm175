﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MidTermProject.GroupStudent
{
    public partial class Update_GroupStudent : Form
    {
        int gid, sid;
        public Update_GroupStudent(int GID, int SID, int status, string date)
        {
            InitializeComponent();
            group.Items.Add(GID);
            gid = GID;
            student.Items.Add(SID);
            sid = SID;
            dateTimePicker1.Value = DateTime.Parse(date);
        }

        private void button2_Click(object sender, EventArgs e) // update
        {
            if(gid != null)
            {
                int status_option = 0;
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update groupStudent set Status = @status, AssignmentDate = @AssignmentDate where GroupId = @GroupId AND StudentId = @StudentId", con);

                    cmd.Parameters.AddWithValue("@GroupId", gid);
                    cmd.Parameters.AddWithValue("@StudentId", sid);

                    if (status.SelectedItem == "Active")
                    {
                        status_option = 3;
                    }

                    if (status.SelectedItem == "InActive")
                    {
                        status_option = 4;
                    }
                    cmd.Parameters.AddWithValue("@Status", status_option);
                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePicker1.Text);

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully update");

                    Form f = new View_GroupStudent();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
            
        }

        private void Update_GroupStudent_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void button1_Click(object sender, EventArgs e) // back
        {
            Form f = new View_GroupStudent();
            this.Hide();
            f.Show();
        }
    }
}
