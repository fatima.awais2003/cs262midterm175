﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MidTermProject.GroupStudent
{
    public partial class View_GroupStudent : Form
    {
        int GID = 0, SID = 0, Status = 0;
        string date;
        public View_GroupStudent()
        {
            InitializeComponent();
        }

        private void View_GroupStudent_Load(object sender, EventArgs e) // view
        {
            this.WindowState = FormWindowState.Maximized;
            dataGridView1.ReadOnly = true;
            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select G.StudentId, G.GroupId, G.Status, G.AssignmentDate \r\n from GroupStudent as G \r\n where G.GroupId IN(select G.GroupId \r\n from GroupStudent as G \r\n where G.Status = 3 \r\n group by G.GroupId)");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void button1_Click(object sender, EventArgs e) // back
        {
            Form f = new Form1();
            this.Hide();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e) // reload data
        {
            dataGridView1.ReadOnly = true;
            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select G.StudentId, G.GroupId, G.Status, G.AssignmentDate \r\n from GroupStudent as G \r\n where G.GroupId IN(select G.GroupId \r\n from GroupStudent as G \r\n where G.Status = 3 \r\n group by G.GroupId)");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void button3_Click(object sender, EventArgs e) // update
        {
            if (date == null || GID == 0 || SID == 0 || Status == 0)
            {
                MessageBox.Show("Select a record");
            }
            if (date != null || GID != 0 || SID != 0 || Status != 0)
            {
                try
                {
                    Form f = new Update_GroupStudent(GID, SID, Status, date);
                    this.Hide();
                    f.Show();
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }

            
        }

        private void button4_Click(object sender, EventArgs e) // delete
        {
            if (SID != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update GroupStudent set Status = 4 where GroupId  = @GroupId");
                    cmd.Parameters.AddWithValue("@GroupId", GID);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");

                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }

            else
            {
                MessageBox.Show("Select a record to delete");
            }

        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            GID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
            Status = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());
            date = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
        }
    }
}
