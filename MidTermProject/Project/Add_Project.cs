﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MidTermProject.Project
{
    public partial class Add_Project : Form
    {
        public Add_Project()
        {
            InitializeComponent();
        }

        private void Add_Project_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form f = new Form1();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e) // add a project
        {
            bool flag = true;

            if(flag == true)
            {
                if(string.IsNullOrEmpty(description.Text) || string.IsNullOrEmpty(title.Text))
                {
                    flag = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            try
            {
                if(flag == true)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO Project (Description, Title) VALUES (@Description, @Title)", con);
                    cmd.Parameters.AddWithValue("@Description", description.Text);
                    cmd.Parameters.AddWithValue("@Title", title.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");
                    this.Hide();
                    Form f = new Form1();
                    f.Show();
                }

            }

            catch(Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }
    }
}
