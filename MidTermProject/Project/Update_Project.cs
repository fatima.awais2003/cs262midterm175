﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidTermProject.Project
{
    public partial class Update_Project : Form
    {
        public int ID_text;
        public Update_Project(int ID, string Description, string Title)
        {
            InitializeComponent();
            ID_text = ID;
            description.Text = Description;
            title.Text = Title;
        }

        private void button2_Click(object sender, EventArgs e) // update
        {
            bool check = true;

            if(check == true)
            {
                if(string.IsNullOrEmpty(description.Text) || string.IsNullOrEmpty(title.Text))
                {
                    MessageBox.Show("Fill fields");
                    check = false;
                }
            }

            if(ID_text != 0 && check == true)
            {
                try
                {

                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update Project " +
                        " set Description = @Description, Title = @Title where Id = @ID ", con);
                    cmd.Parameters.AddWithValue("@Description", description.Text);
                    cmd.Parameters.AddWithValue("@Title", title.Text);
                    cmd.Parameters.AddWithValue("@ID", ID_text);


                    cmd.ExecuteNonQuery();

                    MessageBox.Show("Successfully update");
                    Form f = new View_Projects();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form f = new View_Projects();
            f.Show();
        }

        private void Update_Project_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }
    }
}
