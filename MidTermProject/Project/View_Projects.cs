﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MidTermProject.Project
{
    public partial class View_Projects : Form
    {
        public string description, title;
        public int ID;
        public View_Projects()
        {
            InitializeComponent();
        }

        private void View_Projects_Load(object sender, EventArgs e) // view records
        {
            this.WindowState = FormWindowState.Maximized;
            dataGridView1.ReadOnly = true;
            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id, Description, Title \r\nfrom Project");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void button4_Click(object sender, EventArgs e) // back
        {
            Form f = new Form1();
            f.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e) // reload data
        {
            dataGridView1.ReadOnly = true;
            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id, Description, Title \r\nfrom Project");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void button3_Click(object sender, EventArgs e) // delete a record
        {
            if (ID != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Delete From Project Where Id = @Id");
                    cmd.Parameters.AddWithValue("@Id", ID);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");

                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }

            else
            {
                MessageBox.Show("Select a record to delete");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (description == null || title == null)
            {
                MessageBox.Show("Select a record");
            }


            if(ID != 0 || description != null || title != null)
            {
                try
                {
                    Form f = new Update_Project(ID, description, title);
                    this.Hide();
                    f.Show();
                }

                catch(Exception error)
                {
                    MessageBox.Show(error.Message);
                }
                
            }
           
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e) // selecting a tuple
        {
            ID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            description = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            title = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
        }
    }
}
