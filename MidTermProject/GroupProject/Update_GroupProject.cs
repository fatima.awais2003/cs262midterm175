﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidTermProject.GroupProject
{
    public partial class Update_GroupProject : Form
    {
        public int groupId;
        public Update_GroupProject(int PID, int GID, string date)
        {
            InitializeComponent();
            groupId = GID;
            group.Items.Add(groupId);
            dateTimePicker1.Value = DateTime.Parse(date);
        }

        private void button1_Click(object sender, EventArgs e) // back
        {
            Form f = new View_GroupProject();
            this.Hide();
            f.Show();
        }

        private void project_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from Project";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Project");
                    project.DisplayMember = "Id";
                    project.ValueMember = "Id";
                    project.DataSource = ds.Tables["Project"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e) // update
        {
            bool check = true;

            if (check == true)
            {
                if (string.IsNullOrEmpty(group.Text) || string.IsNullOrEmpty(project.Text))
                {
                    MessageBox.Show("Fill fields");
                    check = false;
                }
            }

            if (check == true)
            {
                try
                {

                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update GroupProject set ProjectId = @ProjectId where GroupId = @GroupId ", con);
                    cmd.Parameters.AddWithValue("@ProjectId", project.SelectedValue);
                    cmd.Parameters.AddWithValue("@GroupId", groupId);
                    cmd.ExecuteNonQuery();

                    MessageBox.Show("Successfully update");
                    Form f = new View_GroupProject();
                    this.Hide();
                    f.Show();
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Assignment Date cannot be changed");
                }
            }
        }

        private void Update_GroupProject_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }
    }
}
