﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidTermProject.GroupProject
{
    public partial class View_GroupProject : Form
    {
        public int GID, PID;
        public string date;
        public View_GroupProject()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) // back
        {
            this.Hide();
            Form f = new Form1();
            f.Show();
        }

        private void View_GroupProject_Load(object sender, EventArgs e) // view at load
        {
            this.WindowState = FormWindowState.Maximized;
            dataGridView1.ReadOnly = true;
            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;


            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select ProjectId, GroupId, AssignmentDate from GroupProject");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.ReadOnly = true;
            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;


            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select GroupId, ProjectId, AssignmentDate from GroupProject");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void button4_Click(object sender, EventArgs e) // delete
        {
            if (GID != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("delete from GroupProject where ProjectId = @ProjectId AND GroupId = @GroupId");
                    cmd.Parameters.AddWithValue("@GroupId", GID);
                    cmd.Parameters.AddWithValue("@ProjectId", PID);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");

                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
            else
            {
                MessageBox.Show("Select a record");
            }
        }

        private void button3_Click(object sender, EventArgs e) // update
        {
            if(PID != 0 && GID != 0)
            {
                Form f = new Update_GroupProject(PID, GID, date);
                this.Hide();
                f.Show();
            }
            else
            {
                MessageBox.Show("Select a record");
            }
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            PID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            GID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
            date = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
        }
    }
}
