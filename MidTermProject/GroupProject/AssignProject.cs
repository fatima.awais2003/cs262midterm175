﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidTermProject.GroupProject
{
    public partial class AssignProject : Form
    {
        public AssignProject()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) // back
        {
            this.Hide();
            Form f = new Form1();
            f.Show();
        }

        private void AssignProject_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void button2_Click(object sender, EventArgs e) // add
        {
            bool check = true;

            if (check == true)
            {
                if (string.IsNullOrEmpty(dateTimePicker1.Text) || string.IsNullOrEmpty(project.Text) || string.IsNullOrEmpty(group.Text))
                {
                    check = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            try
            {
                if (check == true)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO GroupProject(GroupId,ProjectId,AssignmentDate) VALUES (@GroupId,@ProjectId,@AssignmentDate)", con);
                    cmd.Parameters.AddWithValue("@GroupId", group.SelectedValue);
                    cmd.Parameters.AddWithValue("@ProjectId", project.SelectedValue);

                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePicker1.Text);

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");

                    Form f = new Form1();
                    this.Hide();
                    f.Show();
                }
            }

            catch (Exception error)
            {
                MessageBox.Show("Project Assigned");
            }
        }

        private void project_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from Project";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Project");
                    project.DisplayMember = "Id";
                    project.ValueMember = "Id";
                    project.DataSource = ds.Tables["Project"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void group_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from [Group]";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "[Group]");
                    group.DisplayMember = "Id";
                    group.ValueMember = "Id";
                    group.DataSource = ds.Tables["[Group]"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
