﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms; 

namespace MidTermProject
{
    public partial class Generate_Reports : Form
    {

        public Generate_Reports()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            this.Hide();
            f.Show();
        }

        private void Generate_Reports_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String connection = @"Data Source=(local);Initial Catalog=ProjectA;Integrated Security=True";
            string query = "select GS.StudentId as [Student ID], PA.ProjectId as [Project ID], PA.AdvisorId as [Advisor Id], L.Value as [Advisor Role] from GroupStudent as GS join GroupProject as GP on GP.GroupId = GS.GroupId join ProjectAdvisor as PA on PA.ProjectId = GP.ProjectId join Lookup as L on PA.AdvisorRole = L.Id";
            PDF_Report.GeneratePDF(connection, query);
            MessageBox.Show("PDF Report has been generated");

            Form f = new Form1();
            this.Hide();
            f.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String connection = @"Data Source=(local);Initial Catalog=ProjectA;Integrated Security=True";
            string query = "select GS.StudentId as [Student ID], GP.ProjectId as [Project ID], e.Name as [Exam Name], E.TotalMarks as [Total Marks], GE.ObtainedMarks as [Obtained Marks] from GroupStudent as GS join GroupProject as GP on GS.GroupId = GP.GroupId join GroupEvaluation as GE on GE.GroupId = GP.GroupId join Evaluation as E on E.Id = GE.EvaluationId";
            PDF_Report.GeneratePDF2(connection, query);
            MessageBox.Show("PDF Report has been generated");

            Form f = new Form1();
            this.Hide();
            f.Show();
        }
    }
}
//select GS.StudentId, GP.ProjectId, e.Name, E.TotalMarks, GE.ObtainedMarks from GroupStudent as GS join GroupProject as GP on GS.GroupId = GP.GroupId join GroupEvaluation as GE on GE.GroupId = GP.GroupId join Evaluation as E on E.Id = GE.EvaluationId