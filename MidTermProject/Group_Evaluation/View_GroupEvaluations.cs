﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidTermProject.Group_Evaluation
{
    public partial class View_GroupEvaluations : Form
    {
        int GID, EID, Marks;
        string date;
        public View_GroupEvaluations()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) // back
        {
            Form f = new Form1();
            this.Hide();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e) // reload
        {
            dataGridView1.ReadOnly = true;
            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select GroupId, EvaluationId, ObtainedMarks, EvaluationDate from GroupEvaluation");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }


        private void button3_Click(object sender, EventArgs e) // update
        {
            if (date == null || GID == 0 || EID == 0 || Marks == 0)
            {
                MessageBox.Show("Select a record");
            }
            if (date != null && GID != 0 && EID != 0 && Marks != 0)
            {
                try
                {
                    Form f = new Update_GroupEvaluation(GID, EID, Marks, date);
                    this.Hide();
                    f.Show();
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }

        private void dataGridView1_RowHeaderMouseClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
            GID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            EID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
            Marks = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());
            date = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        private void View_GroupEvaluations_Load(object sender, EventArgs e) // view
        {
            this.WindowState = FormWindowState.Maximized;
            dataGridView1.ReadOnly = true;

            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select GroupId, EvaluationId, ObtainedMarks, EvaluationDate from GroupEvaluation");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void button4_Click(object sender, EventArgs e) // delete
        {
            if(GID != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Delete From GroupEvaluation Where GroupId = @GroupId AND EvaluationId= @EvaluationId");
                    cmd.Parameters.AddWithValue("@GroupId", GID);
                    cmd.Parameters.AddWithValue("@EvaluationId", EID);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");

                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
            else
            {
                MessageBox.Show("Select a record");
            }
        }
    }
}
