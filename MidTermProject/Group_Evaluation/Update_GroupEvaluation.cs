﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MidTermProject.Group_Evaluation
{
    public partial class Update_GroupEvaluation : Form
    {
        int gid, eid;
        public Update_GroupEvaluation(int GID, int EID, int Obtained_marks, string date)
        {
            InitializeComponent();
            
            gid = GID;
            group.Items.Add(GID);
            eid = EID;
            evaluation.Items.Add(eid);
            marks.Text = Obtained_marks.ToString();
            dateTimePicker1.Value = DateTime.Parse(date);
        }

        private void Update_GroupEvaluation_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void button1_Click(object sender, EventArgs e) // back
        {
            this.Hide();
            Form f = new View_GroupEvaluations();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e) // update
        {
            if (gid != null)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update GroupEvaluation set ObtainedMarks = @ObtainedMarks, EvaluationDate = @EvaluationDate where GroupId = @GroupId AND EvaluationId = @EvaluationId", con);

                    cmd.Parameters.AddWithValue("@GroupId", gid);
                    cmd.Parameters.AddWithValue("@EvaluationId", eid);

                    cmd.Parameters.AddWithValue("@ObtainedMarks", int.Parse(marks.Text));
                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@EvaluationDate", dateTimePicker1.Text);

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully update");

                    Form f = new View_GroupEvaluations();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }
    }
}
