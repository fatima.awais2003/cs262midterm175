﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MidTermProject.Group_Evaluation
{
    public partial class Add_GroupEvaluation : Form
    {
        public Add_GroupEvaluation()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Add_GroupEvaluation_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }


        private void group_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "select distinct (G.GroupId) \r\n from GroupStudent as G \r\n where G.GroupId IN(select G.GroupId \r\n from GroupStudent as G \r\n where G.Status = 3 \r\n group by G.GroupId)";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "GroupStudent");
                    group.DisplayMember = "GroupId";
                    group.ValueMember = "GroupId";
                    group.DataSource = ds.Tables["GroupStudent"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void evaluation_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from Evaluation";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Evaluation");
                    evaluation.DisplayMember = "Id";
                    evaluation.ValueMember = "Id";
                    evaluation.DataSource = ds.Tables["Evaluation"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form f = new Form1();
            f.Show();
        }

        private void button2_Click_1(object sender, EventArgs e) // add
        {
            bool check = true;

            if (check == true)
            {
                if (string.IsNullOrEmpty(dateTimePicker1.Text) || string.IsNullOrEmpty(marks.Text) || string.IsNullOrEmpty(evaluation.Text) || string.IsNullOrEmpty(group.Text))
                {
                    check = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            try
            {
                if (check == true)
                {
                    try
                    {
                        var con1 = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("INSERT INTO GroupEvaluation(GroupId,EvaluationId,ObtainedMarks,EvaluationDate) VALUES (@GroupId,@EvaluationId, @ObtainedMarks,@EvaluationDate)", con1);
                        cmd.Parameters.AddWithValue("@GroupId", group.SelectedValue);
                        cmd.Parameters.AddWithValue("@EvaluationId", evaluation.SelectedValue);

                        string connectionString = @"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True";
                        int value;

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {

                            connection.Open();

                            string query = "select Evaluation.TotalMarks from Evaluation where Id = @Id";
                            SqlCommand command = new SqlCommand(query, connection);
                            command.Parameters.AddWithValue("@Id", evaluation.SelectedValue);

                            object result = command.ExecuteScalar();

                            if (result != null && result != DBNull.Value)
                            {
                                value = (int)result;
                            }
                            else
                            {
                                value = 0; // or any other default value you want to use

                            }
                        }

                        if (value >= int.Parse(marks.Text))
                        {

                            cmd.Parameters.AddWithValue("@ObtainedMarks", int.Parse(marks.Text));
                        }
                        else
                        {
                            MessageBox.Show("Enter correct marks");
                        }

                        dateTimePicker1.Format = DateTimePickerFormat.Custom;
                        dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                        cmd.Parameters.AddWithValue("@EvaluationDate", dateTimePicker1.Text);

                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Successfully saved");

                        Form f = new Form1();
                        this.Hide();
                        f.Show();
                    }

                    catch
                    {
                        MessageBox.Show("Incorrect data entered");
                    }

                }
            }

            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }
    }
}
