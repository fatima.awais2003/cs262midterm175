﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidTermProject.Evaluation
{
    public partial class Update_Evaluation : Form
    {
        public int ID_text;
        public Update_Evaluation(int ID, string Name, int Marks, int Weightage)
        {
            InitializeComponent();
            ID_text = ID;
            name.Text = Name;
            marks.Text = Marks.ToString();
            weightage.Text = Weightage.ToString();
        }

        private void button1_Click(object sender, EventArgs e) // update
        {
            if (ID_text != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update Evaluation " +
                        " set Name = @Name, TotalMarks = @TotalMarks, TotalWeightage = @TotalWeightage where Id = @ID ", con);

                    cmd.Parameters.AddWithValue("@ID", ID_text);
                    cmd.Parameters.AddWithValue("@Name", name.Text);
                    cmd.Parameters.AddWithValue("@TotalMarks", int.Parse(marks.Text));
                    cmd.Parameters.AddWithValue("@TotalWeightage", int.Parse(weightage.Text));
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully update");

                    Form f = new View_Evaluation();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }

        }

        private void button2_Click(object sender, EventArgs e) // back
        {
            this.Hide();
            Form f = new View_Evaluation();
            f.Show();
        }

        private void Update_Evaluation_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }
    }
}
