﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MidTermProject.Evaluation
{
    public partial class Add_Evaluation : Form
    {
        public Add_Evaluation()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) // add evaluation
        {
            bool check = true;
            if (check == true)
            {
                if (string.IsNullOrEmpty(name_text.Text) || string.IsNullOrEmpty(weightage_text.Text) || string.IsNullOrEmpty(marks_text.Text))
                {
                    check = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            try
            {
                if (check == true)
                {
                    if(int.Parse(marks_text.Text) <= 100 && int.Parse(weightage_text.Text) <= 100)
                    {
                        try
                        {
                            var con = Configuration.getInstance().getConnection();
                            SqlCommand cmd = new SqlCommand("INSERT INTO Evaluation(Name,TotalMarks,TotalWeightage) VALUES (@Name,@TotalMarks,@TotalWeightage)", con);
                            cmd.Parameters.AddWithValue("@Name", name_text.Text);
                            cmd.Parameters.AddWithValue("@TotalMarks", marks_text.Text);
                            cmd.Parameters.AddWithValue("@TotalWeightage", weightage_text.Text);
                            cmd.ExecuteNonQuery();
                            MessageBox.Show("Successfully saved");

                            Form f = new Form1();
                            this.Hide();
                            f.Show();
                        }

                        catch(Exception ex)
                        {
                            MessageBox.Show("Enter valid numbers");
                        }
                    }

                    else
                    {
                        MessageBox.Show("Enter valid numbers");
                    }
                    
                }
            }

            catch (Exception error)
            {
                MessageBox.Show("character input");
            }
        }

        private void Add_Evaluation_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            this.Hide();
            f.Show();
        }
    }
}
