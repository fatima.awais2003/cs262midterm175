﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MidTermProject.Evaluation
{
    public partial class View_Evaluation : Form
    {
        public int ID, Marks, Weightage;
        public string Name;
        public View_Evaluation()
        {
            InitializeComponent();
        }

        private void View_Evaluation_Load(object sender, EventArgs e) // view data in table
        {
            this.WindowState = FormWindowState.Maximized;
            dataGridView1.ReadOnly = true;
            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id, Name, TotalMarks, TotalWeightage \r\nfrom Evaluation");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void button1_Click(object sender, EventArgs e) // reload 
        {
            dataGridView1.ReadOnly = true;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id, Name, TotalMarks, TotalWeightage \r\nfrom Evaluation");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            Name = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            Marks = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());
            Weightage = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());
        }

        private void button4_Click(object sender, EventArgs e) // back
        {
            this.Hide();
            Form f = new Form1();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e) // update
        {
            if (Name == null || Marks == null || Weightage == null)
            {
                MessageBox.Show("Select a record");
            }
            if (Name != null || Marks != null || Weightage != null)
            {
                Form f = new Update_Evaluation(ID, Name, Marks, Weightage);
                this.Hide();
                f.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e) // delete a record
        {
            if (ID != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Delete From Evaluation Where Id = @Id");
                    cmd.Parameters.AddWithValue("@Id", ID);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");

                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }

            else
            {
                MessageBox.Show("Select a record to delete");
            }
        }
    }
}
