﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;


namespace MidTermProject.Students_CRUD
{
    public partial class View_Students : Form
    {
        public int ID = 0, Gender;
        public string FN, LN, Email, Contact, RegNo, DOB;
        private void button2_Click_1(object sender, EventArgs e) // update a record
        {
            if(RegNo == null || FN == null || LN == null || Email == null || Contact == null)
            {
                MessageBox.Show("Select a record");
            }
            if(RegNo != null || FN != null || LN != null || Email != null || Contact != null)
            {
                try
                {
                    Form f = new Update_Student(ID, RegNo, Gender, FN, LN, Email, Contact, DOB);
                    this.Hide();
                    f.Show();
                }
                catch(Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
            
        }

        private void dataGridView1_RowHeaderMouseClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            RegNo = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            FN = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            LN = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            Contact = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            DOB = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
            Email = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();

            if (dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString() == "Male")
            {
                Gender = 1;
            }
            else if (dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString() == "Female")
            {
                Gender = 2;
            }


        }

        public View_Students()
        {
            InitializeComponent();
        }

        private void View_Students_Load(object sender, EventArgs e) // view students
        {
            this.WindowState = FormWindowState.Maximized;
            dataGridView1.ReadOnly = true;
            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Std.Id , Std.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.DateOfBirth , P.Email , LU.Value as Gender \r\nfrom Student as Std\r\njoin Person as P\r\n on Std.Id = P.Id \r\n join Lookup as LU \r\non LU.Id = P.Gender");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            Form f = new Form1();
            f.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e) // reload data in table
        {
            dataGridView1.ReadOnly = true;
            int totalRowHeight;

            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            totalRowHeight = dataGridView1.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalRowHeight += row.Height;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Std.Id , Std.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.DateOfBirth , P.Email , LU.Value as Gender \r\nfrom Student as Std\r\njoin Person as P\r\n on Std.Id = P.Id \r\n join Lookup as LU \r\non LU.Id = P.Gender");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void button3_Click_1(object sender, EventArgs e) //delete a record from table
        {
            if (ID != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Delete From Student Where Id = @Id");
                    //SqlCommand cmd1 = new SqlCommand("Delete From Person Where Id = @Id");
                    cmd.Parameters.AddWithValue("@Id", ID);
                    //cmd1.Parameters.AddWithValue("@Id", ID);
                    cmd.Connection = con;
                    //cmd1.Connection = con;
                    cmd.ExecuteNonQuery();
                    //cmd1.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");

                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }

            else
            {
                MessageBox.Show("Select a record to delete");
            }

        }

    }
}
