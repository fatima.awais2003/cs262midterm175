﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using MidTermProject.Students_CRUD;
using System.Windows.Forms;


namespace MidTermProject.Students_CRUD
{
    public partial class Student : Form
    {
        public Student()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form f = new Form1();
            f.Show();
        }

        private void Student_Load_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }
        private void button2_Click_1(object sender, EventArgs e) // add student
        {
            bool check = true;
            int gender = -1;

            if (check == true)
            {
                if (string.IsNullOrEmpty(regNo.Text) || string.IsNullOrEmpty(firstName.Text) || string.IsNullOrEmpty(lastName.Text) || string.IsNullOrEmpty(contact.Text) || string.IsNullOrEmpty(email.Text))
                {
                    check = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            try
            {
                if (check == true)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO Person(FirstName,LastName,Contact,Email,DateOfBirth,Gender) VALUES (@FirstName,@LastName, @Contact,@Email,@DOB, @Gender)", con);
                    SqlCommand cmd1 = new SqlCommand("INSERT INTO Student(Id, RegistrationNo) VALUES ((SELECT Id FROM Person WHERE FirstName = @FirstName AND LastName=@LastName AND Contact=@Contact AND Email=@Email AND DateOfBirth=@DOB AND Gender=@Gender) ,@RegistrationNo)", con);
                    cmd.Parameters.AddWithValue("@FirstName", firstName.Text);
                    cmd.Parameters.AddWithValue("@LastName", lastName.Text);
                    cmd.Parameters.AddWithValue("@Contact", contact.Text);
                    cmd.Parameters.AddWithValue("@Email", email.Text);
                    cmd1.Parameters.AddWithValue("@FirstName", firstName.Text);
                    cmd1.Parameters.AddWithValue("@LastName", lastName.Text);
                    cmd1.Parameters.AddWithValue("@Contact", contact.Text);
                    cmd1.Parameters.AddWithValue("@Email", email.Text);
                    cmd1.Parameters.AddWithValue("@RegistrationNo", regNo.Text);

                    if (genderText.SelectedItem == "Male")
                    {
                        gender = 1;
                    }
                    if (genderText.SelectedItem == "Female")
                    {
                        gender = 2;
                    }

                    dateTimePicker.Format = DateTimePickerFormat.Custom;
                    dateTimePicker.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@Gender", gender);
                    cmd.Parameters.AddWithValue("@DOB", dateTimePicker.Text);
                    cmd1.Parameters.AddWithValue("@Gender", gender);
                    cmd1.Parameters.AddWithValue("@DOB", dateTimePicker.Text);
                    cmd.ExecuteNonQuery();
                    cmd1.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");

                    Form f = new Form1();
                    this.Hide();
                    f.Show();
                }
            }

            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }
    }
}
