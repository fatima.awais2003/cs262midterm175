﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidTermProject.Students_CRUD
{
    public partial class Update_Student : Form
    {
        public int ID_text;
        public Update_Student(int ID, string RegNo, int Gender, string FN, string LN, string Email, string Contact, string DOB)
        {
            InitializeComponent();
            ID_text = ID;
            regNo.Text = RegNo;
            genderText.SelectedIndex = Gender - 1;
            firstName.Text = FN;
            lastName.Text = LN;
            email.Text = Email;
            contact.Text = Contact;
            dateTimePicker.Value = DateTime.Parse(DOB);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form f = new View_Students();
            this.Hide();
            f.Show();
        }

        private void Update_Student_Load(object sender, EventArgs e)
        {
            
        }


        private void button2_Click_1(object sender, EventArgs e) // updating a record
        {
            if (ID_text != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update Person " +
                        " set FirstName = @FirstName, LastName = @LastName, Contact = @Contact, Email = @Email, DateOfBirth = @DOB, Gender = @Gender where Id = @ID " +
                        " update Student set RegistrationNo = @RegistrationNumber where Id = @ID ", con);
                    cmd.Parameters.AddWithValue("@FirstName", firstName.Text);
                    cmd.Parameters.AddWithValue("@LastName", lastName.Text);
                    cmd.Parameters.AddWithValue("@Contact", contact.Text);
                    cmd.Parameters.AddWithValue("@Email", email.Text);
                    cmd.Parameters.AddWithValue("@ID", ID_text);
                    cmd.Parameters.AddWithValue("@RegistrationNumber", regNo.Text);
                    cmd.Parameters.AddWithValue("@Gender", genderText.SelectedIndex + 1);
                    dateTimePicker.Format = DateTimePickerFormat.Custom;
                    dateTimePicker.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@DOB", dateTimePicker.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully update");

                    Form f = new View_Students();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }

        }
    }
}
